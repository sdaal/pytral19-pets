(function(window, document, undefined){
  function initPage(event) {
    const form = document.querySelector('.pet-form');
    const formsetContainer = document.querySelector('.formset-container');

    form.addEventListener("click", function (event) {
      const elem = event.target;
      const hasAction = elem.dataset.action != null;

      if (elem.tagName === 'BUTTON' && hasAction) {
        const action = elem.dataset.action;
        const table = elem.closest('table');
        const tbody = table.querySelector('tbody');
        const emptyForm = table.querySelector('tbody > tr.empty-form').cloneNode(true);

        if (action === 'remove') {
          event.target.parentElement.remove();
        } else if (action === 'add') {

        }
      }
    });
  }

  window.onload = initPage;
})(window, document);
