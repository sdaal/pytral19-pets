from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Pet


class PetListOrCreateView(APIView):
    def get(self, request, format=None):
        pets = Pet.objects.all()
        return Response({
            "items": [
                {"id": pet.pk, "name": pet.name, "age": pet.age, "gender": pet.gender}
                for pet in pets
            ]
        })

    def post(self, request, format=None):
        pet = Pet.objects.create(**request.data)
        return Response({
            "id": pet.pk,
            "name": pet.name,
            "age": pet.age,
            "gender": pet.gender,
            "bio": pet.bio,
        }, status=status.HTTP_201_CREATED)


class PetDetailOrUpdateOrDeleteView(APIView):
    def get(self, request, pk, format=None):
        try:
            pet = Pet.objects.get(pk=pk)
        except Pet.DoesNotExist:
            return Response({"message": "Pet not found"}, status=status.HTTP_404_NOT_FOUND)
        return Response({"name": pet.name, "id": pet.pk, "age": pet.age, "gender": pet.gender, "bio": pet.bio})

    def put(self, request, pk, format=None):
        return Response({"message": "PUT"})

    def delete(self, request, pk, format=None):
        return Response({"message": "DELETE"})
