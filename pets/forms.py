from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Button
from django import forms
from django.forms import inlineformset_factory

from .models import Pet, Photo


class PetForm(forms.ModelForm):
    class Meta:
        model = Pet
        fields = ['name', 'age', 'gender', 'bio']


class PhotoInlineForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ['picture', 'caption']


PhotoFormSet = inlineformset_factory(
    Pet,
    Photo,
    extra=1,
    form=PhotoInlineForm,
    min_num=1,
    max_num=10,
)


class PhotoFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.template = 'partials/table_inline_formset.html'
