from django.conf import settings
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from imagekit.processors import ResizeToFill
from imagekit.models import ImageSpecField


class Pet(models.Model):
    class Gender(models.TextChoices):
        MALE = 'M', _('Male')
        FEMALE = 'F', _('Female')

    name = models.CharField(_("name"), max_length=64)
    # slug = models.SlugField(_("slug"))
    age = models.IntegerField(_("age"))
    gender = models.CharField(_("gender"), choices=Gender.choices, max_length=1)
    bio = models.TextField(_("bio"), blank=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                              on_delete=models.SET_NULL, verbose_name=_("owner"),
                              related_name="pets")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'pets'   # by default: pets_pet
        verbose_name = _("pet")
        verbose_name_plural = _("pets")
        constraints = [
            models.CheckConstraint(check=Q(age__gte=0) & Q(age__lte=20),
                                   name="chk_pets_age_positive"),
        ]
        # indexes = [
        #     models.Index(fields=["slug"], name="chk_pets_slug"),
        # ]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('pets:pet_detail', kwargs={'pk': self.pk})


class Photo(models.Model):
    picture = models.ImageField(_("picture"), upload_to="pets")
    picture_lg = ImageSpecField(source="picture", processors=[
        ResizeToFill(640, 480),
    ], format="JPEG", options={"quality": 70})
    picture_sm = ImageSpecField(source="picture", processors=[
        ResizeToFill(240, 180),
    ], format="JPEG", options={"quality": 70})
    caption = models.CharField(_("caption"), max_length=255, blank=True)
    pet = models.ForeignKey(Pet, verbose_name=_("pet"), on_delete=models.CASCADE, related_name="photos")

    class Meta:
        db_table = 'photos'
        verbose_name = _("photo")
        verbose_name_plural = _("photos")

    def __str__(self):
        if self.caption:
            return self.caption
        return self.picture.name


class Like(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, related_name="likes",
                             verbose_name=_("user"))
    pet = models.ForeignKey(Pet, verbose_name=_("pet"),
                            on_delete=models.CASCADE, related_name="likes")
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'likes'
        verbose_name = _("like")
        verbose_name_plural = _("likes")
        constraints = [
            models.UniqueConstraint(fields=["user", "pet"], name="uix_likes_users_pets"),
        ]
        # unique_together = ("user", "pet")
