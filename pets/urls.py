from django.urls import path

from . import views
from .apiviews import PetListOrCreateView, PetDetailOrUpdateOrDeleteView

app_name = 'pets'

urlpatterns = [
    path('', views.PetListView.as_view(), name='pet_list'),
    path('<int:pk>/', views.PetDetailView.as_view(), name='pet_detail'),
    path('<int:pk>/update/', views.PetUpdateView.as_view(), name='pet_update'),
    path('<int:pk>/delete/', views.PetDeleteView.as_view(), name='pet_delete'),
    path('add/', views.PetCreateView.as_view(), name='pet_create'),
    path("pets/", PetListOrCreateView.as_view(), name="api_pet_list"),
    path("pets/<int:pk>/", PetDetailOrUpdateOrDeleteView.as_view(), name="api_pet_detail"),
]
