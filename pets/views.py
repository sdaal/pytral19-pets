from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views import generic

from .forms import PetForm, PhotoFormSet, PhotoFormHelper
from .models import Pet


class PetListView(generic.ListView):
    queryset = Pet.objects.order_by('-created_at').all()
    template_name = 'pets/pet_list.html'
    context_object_name = 'pets'
    paginate_by = 10


class PetDetailView(generic.DetailView):
    model = Pet
    # pk_url_kwarg = 'pk'
    # context_object_name = 'pet'


class PetCreateView(LoginRequiredMixin, generic.CreateView):
    model = Pet
    form_class = PetForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        opts = {}
        if self.request.method == 'POST':
            opts.update({
                "data": self.request.POST,
                "files": self.request.FILES,
            })
        context['formset'] = PhotoFormSet(**opts)
        context['helper'] = PhotoFormHelper()
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        formset = PhotoFormSet(self.request.POST, files=self.request.FILES,
                               instance=self.object)
        if formset.is_valid():
            formset.save()
        return super().form_valid(form)


class PetUpdateView(generic.UpdateView):
    model = Pet
    form_class = PetForm


class PetDeleteView(generic.DeleteView):
    model = Pet
    success_url = reverse_lazy('pets:pet_list')
